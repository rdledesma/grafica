import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib as mpl
archivo_excel = pd.read_excel('./GRAFICO_BSAS.xlsx')

KtgrafData = archivo_excel[archivo_excel.columns[0]].values
KdgrafData= archivo_excel[archivo_excel.columns[1]].values
#
#
x = KtgrafData
y = KdgrafData


color = [item*255 for item in y*x]
plot = plt.scatter(x,y, label="Grafico .... Nombre...", s=1, c=color)
plt.legend()
plt.title("..Titulo del gráfico...")
plt.xlabel("...KtgrafData..")
plt.ylabel("..KdgrafData..")
plt.colorbar()
plt.show()


